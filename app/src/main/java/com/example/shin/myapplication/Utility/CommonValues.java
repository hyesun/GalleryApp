package com.example.shin.myapplication.Utility;

/**
 * Created by hyesun on 2017-01-13.
 */

public interface CommonValues
{
    final String FILE_PATH = MyApplication.mContext.getFilesDir().getPath();
    final String ORIGIN = "origin";
    final String THUMB = "thumb";
    final String RESIZE = "resize";
}
