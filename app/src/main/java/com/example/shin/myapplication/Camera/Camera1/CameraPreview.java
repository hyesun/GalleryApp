package com.example.shin.myapplication.Camera.Camera1;

import android.content.Context;
import android.hardware.Camera;
import android.hardware.Camera.Size;
import android.hardware.Camera.Parameters;
import android.util.Log;
import android.view.SurfaceHolder;
import android.view.SurfaceView;


import java.io.IOException;
import java.util.List;

/**
 * Created by hyesun on 2017-01-18.
 */

public class CameraPreview extends SurfaceView implements SurfaceHolder.Callback
{
    private final String TAG = "CameraPreview";

    private boolean isTest = true;

    SurfaceHolder mHolder;
    Size mPreviewSize;
    List<Size> mSupportedPreviewSizes;
    Camera mCamera;

    int previewWidth = 0;
    int previewHeight = 0;
    int pictureWidth = 0;
    int pictureHeight = 0;

    CameraPreview(Context context)
    {
        super(context);

        mHolder = getHolder();
        mHolder.addCallback(this);
        mHolder.setType(SurfaceHolder.SURFACE_TYPE_PUSH_BUFFERS);
    }

    public void setPreviewSize(Camera camera)
    {
        mCamera = camera;
        if (mCamera != null)
        {
            mSupportedPreviewSizes = mCamera.getParameters().getSupportedPreviewSizes();
            int w = mSupportedPreviewSizes.get(0).width;
            int h = mSupportedPreviewSizes.get(0).height;
            mPreviewSize = getOptimalPreviewSize(mSupportedPreviewSizes, w, h);
            requestLayout();
        }
    }

    public void setCameraPreview(Camera camera)
    {

        if (isTest == false)
        {
            setPreviewSize(camera);

            try
            {
                camera.setPreviewDisplay(mHolder);
            }
            catch (IOException exception)
            {
                Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
            }
            Parameters parameters = camera.getParameters();
            parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
            requestLayout();

            camera.setParameters(parameters);
        }
        else
        {
            mCamera = camera;
            try
            {
                camera.setPreviewDisplay(mHolder);
            }
            catch (IOException exception)
            {
                Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
            }

            Parameters parameters = camera.getParameters();

            // 해상도가 가장 큰 사진 이미지 구하기
            for (Size size : parameters.getSupportedPictureSizes())
            {
                Log.e(TAG, "picture size : w:" + size.width + " h:" + size.height);

                if (size.width > pictureWidth)
                {
                    pictureWidth = size.width;
                    pictureHeight = size.height;
                }
            }

            //Picture 비율에 가장 가까운 Preview size 구하기

            double min = Integer.MAX_VALUE;
            double pictureRatio = (double)pictureWidth / pictureHeight;

            for (Size size : parameters.getSupportedPreviewSizes())
            {
                double previewRatio = (double)size.width / size.height;

                if (min > Math.abs(pictureRatio - previewRatio))
                {
                    min = Math.abs(pictureRatio - previewRatio);

                    previewWidth = size.width;
                    previewHeight = size.height;
                }
            }


            for (Size size : parameters.getSupportedPreviewSizes())
            {
                Log.e(TAG, "preview size : w:" + size.width + " h:" + size.height);
            }

            parameters.setPreviewSize(previewWidth, previewHeight);
            parameters.setPictureSize(pictureWidth, pictureHeight);

            requestLayout();

            camera.setParameters(parameters);

            Log.i(TAG, "preview = " + camera.getParameters().getPreviewSize().width + " * " + camera.getParameters().getPreviewSize().height);
            Log.i(TAG, "picture = " + camera.getParameters().getPictureSize().width + " * " + camera.getParameters().getPictureSize().height);

        }

    }

    public void surfaceCreated(SurfaceHolder holder)
    {
        try
        {
            if (mCamera != null)
            {
                mCamera.setPreviewDisplay(holder);
            }
        }
        catch (IOException exception)
        {
            Log.e(TAG, "IOException caused by setPreviewDisplay()", exception);
        }
    }

    public void surfaceDestroyed(SurfaceHolder holder)
    {

    }

    private Size getOptimalPreviewSize(List<Size> sizes, int w, int h)
    {
        final double ASPECT_TOLERANCE = 0.1;
        double targetRatio = (double)w / h;
        if (sizes == null)
        {
            return null;
        }

        Size optimalSize = null;
        double minDiff = Double.MAX_VALUE;

        int targetWidth = w;

        for (Size size : sizes)
        {
            double ratio = (double)size.width / size.height;
            if (Math.abs(ratio - targetRatio) > ASPECT_TOLERANCE)
            {
                continue;
            }

            if (Math.abs(size.width - targetWidth) < minDiff)
            {
                optimalSize = size;
                minDiff = Math.abs(size.width - targetWidth);
            }
        }

        if (optimalSize == null)
        {
            minDiff = Double.MAX_VALUE;
            for (Size size : sizes)
            {
                if (Math.abs(size.width - targetWidth) < minDiff)
                {
                    optimalSize = size;
                    minDiff = Math.abs(size.width - targetWidth);
                }
            }
        }
        return optimalSize;
    }

    public void surfaceChanged(SurfaceHolder holder, int format, int w, int h)
    {
        Parameters parameters = mCamera.getParameters();

        if (isTest == false)
        {
            parameters.setPreviewSize(mPreviewSize.width, mPreviewSize.height);
        }
        else
        {
            parameters.setPreviewSize(previewWidth, previewHeight);
            parameters.setPictureSize(pictureWidth, pictureHeight);
        }

        requestLayout();

        mCamera.setParameters(parameters);
        mCamera.startPreview();
    }

}