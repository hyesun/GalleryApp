package com.example.shin.myapplication.Camera.Camera1;

import android.Manifest;
import android.app.Activity;
import android.app.AlertDialog;
import android.app.Dialog;
import android.app.DialogFragment;
import android.app.Fragment;
import android.content.DialogInterface;
import android.content.pm.PackageManager;
import android.hardware.Camera;
import android.hardware.Camera.CameraInfo;;
import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.v13.app.FragmentCompat;
import android.support.v4.content.ContextCompat;
import android.util.DisplayMetrics;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.Surface;
import android.view.View;

import android.hardware.Camera.*;
import android.view.ViewGroup;
import android.widget.FrameLayout;
import android.widget.ProgressBar;
import android.widget.RelativeLayout;

import com.example.shin.myapplication.Camera.Camera2.Camera2Fragment;
import com.example.shin.myapplication.Camera.Common.ImageSaver;
import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.CommonValues;

/**
 * Created by hyesun on 2017-01-18.
 */

public class CameraFragment extends Fragment implements View.OnClickListener, FragmentCompat.OnRequestPermissionsResultCallback, CommonValues
{
    private static final String TAG = "CameraFragment";

    private static final String FRAGMENT_DIALOG = "dialog";
    private static final int REQUEST_CAMERA_PERMISSION = 1;

    private int mCameraId;
    private CameraInfo info;
    private Camera mCamera;
    private CameraPreview mPreview;

    private Handler mHandler = new Handler();

    private FrameLayout mPreviewLayout;
    private ProgressBar mProgressBar;

    private boolean checkPicture = false;
    private boolean isTest = true;

    public static CameraFragment newInstance()
    {
        return new CameraFragment();
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState)
    {
        View rootView = inflater.inflate(R.layout.fragment_camera, container, false);

        if (ContextCompat.checkSelfPermission(getActivity(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED)
        {
            requestCameraPermission();
            return null;
        }

        return rootView;
    }

    @Override
    public void onViewCreated(final View view, Bundle savedInstanceState)
    {
        view.findViewById(R.id.btn_picture).setOnClickListener(this);
        mPreviewLayout = (FrameLayout)view.findViewById(R.id.frame_layout);
        mProgressBar = (ProgressBar) view.findViewById(R.id.progressbar);
        mPreviewLayout.bringChildToFront(mProgressBar);
    }

    @Override
    public void onResume()
    {
        super.onResume();

        addCamera();
    }

    @Override
    public void onPause()
    {
        super.onPause();

        removeCamera();
    }

    @Override
    public void onDestroyView()
    {
        super.onDestroyView();

        if (mCamera != null)
        {
            mCamera.stopPreview();
            mCamera.release();
        }

    }

    private Camera getCameraInstance()
    {
        Camera camera = null;
        mCameraId = getCameraId();
        try
        {
            camera = Camera.open(mCameraId);
        }
        catch (Exception e)
        {
            e.printStackTrace();
        }

        return camera;
    }

    private int getCameraId()
    {
        int numberOfCameras = Camera.getNumberOfCameras();

        for (int i = 0; i < numberOfCameras; i++)
        {
            info = new CameraInfo();

            Camera.getCameraInfo(i, info);

            if (info.facing != CameraInfo.CAMERA_FACING_FRONT)
            {
                return i;
            }
        }

        return 0;
    }

    private void addCamera()
    {
        if (mCamera == null)
        {
            mCamera = getCameraInstance();
        }
        setCameraDisplayOrientation(getActivity());

        if (mPreview == null)
        {
            mPreview = new CameraPreview(getActivity());
        }

        mPreview.setCameraPreview(mCamera);

        if (isTest == false)
        {
            if (mPreviewLayout.getChildCount() == 1)
            {
                mPreviewLayout.addView(mPreview);
            }
        }
        else
        {
            if (mPreviewLayout.getChildCount() == 1)
            {
                double ratio = (double)mPreview.previewWidth / mPreview.previewHeight;
                DisplayMetrics dm = new DisplayMetrics();
                getActivity().getWindowManager().getDefaultDisplay().getMetrics(dm);

                Log.i(TAG, "display = " + dm.widthPixels + " * " + dm.heightPixels);

                int displayWidth = dm.widthPixels;
                int displayHeight = (int)(displayWidth * ratio);

                Log.i(TAG, "display = " + displayWidth + " * " + displayHeight);

                mPreviewLayout.addView(mPreview, new RelativeLayout.LayoutParams(displayWidth, displayHeight));
            }
        }

        mCamera.startPreview();
    }

    @Override
    public void onClick(View view)
    {
        if (view.getId() == R.id.btn_picture && checkPicture == false)
        {
            mCamera.takePicture(null, null, pictureCallback);
            checkPicture = true;
        }
    }

    private PictureCallback pictureCallback = new PictureCallback()
    {
        @Override
        public void onPictureTaken(byte[] bytes, Camera camera)
        {
            Log.i(TAG, "pictureTaken");
            mHandler.post(new ImageSaver(getActivity(), bytes, mProgressBar, 1));
            checkPicture = false;
            mCamera.startPreview();
        }
    };

    private void requestCameraPermission()
    {
        if (FragmentCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.CAMERA))
        {
            new CameraFragment.ConfirmationDialog().show(getChildFragmentManager(), FRAGMENT_DIALOG);
        }
        else
        {
            FragmentCompat.requestPermissions(this, new String[] { Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);
        }
    }

    private void removeCamera()

    {
        if (mCamera != null)
        {
            mCamera.stopPreview();
            mCamera.setPreviewCallback(null);
            mPreview.getHolder().removeCallback(null);
            mCamera.release();
            mCamera = null;
        }
    }

    private void setCameraDisplayOrientation(Activity activity)
    {
        info = new CameraInfo();
        Camera.getCameraInfo(mCameraId, info);
        int rotation = activity.getWindowManager().getDefaultDisplay().getRotation();
        int degrees = 0;

        switch (rotation)
        {
            case Surface.ROTATION_0:
                degrees = 0;
                break;
            case Surface.ROTATION_90:
                degrees = 90;
                break;
            case Surface.ROTATION_180:
                degrees = 180;
                break;
            case Surface.ROTATION_270:
                degrees = 270;
                break;
        }

        int result = 0;
        if (info.facing != CameraInfo.CAMERA_FACING_FRONT)
        {
            result = (info.orientation - degrees + 360) % 360;
        }

        mCamera.setDisplayOrientation(result);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults)
    {
        if (requestCode == REQUEST_CAMERA_PERMISSION)
        {
            if (grantResults.length != 1 || grantResults[0] != PackageManager.PERMISSION_GRANTED)
            {
                Camera2Fragment.ErrorDialog.newInstance(getString(R.string.request_permission)).show(getChildFragmentManager(), FRAGMENT_DIALOG);
            }
        }
        else
        {
            super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        }
    }

    public static class ErrorDialog extends DialogFragment
    {

        private static final String ARG_MESSAGE = "message";

        public static ErrorDialog newInstance(String message)
        {
            ErrorDialog dialog = new ErrorDialog();
            Bundle args = new Bundle();
            args.putString(ARG_MESSAGE, message);
            dialog.setArguments(args);
            return dialog;
        }

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            final Activity activity = getActivity();
            return new AlertDialog.Builder(activity).setMessage(getArguments().getString(ARG_MESSAGE)).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialogInterface, int i)
                {
                    activity.finish();
                }
            }).create();
        }
    }

    public static class ConfirmationDialog extends DialogFragment
    {

        @Override
        public Dialog onCreateDialog(Bundle savedInstanceState)
        {
            final Fragment parent = getParentFragment();
            return new AlertDialog.Builder(getActivity()).setMessage(R.string.request_permission).setPositiveButton(android.R.string.ok, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    FragmentCompat.requestPermissions(parent, new String[] { Manifest.permission.CAMERA }, REQUEST_CAMERA_PERMISSION);
                }
            }).setNegativeButton(android.R.string.cancel, new DialogInterface.OnClickListener()
            {
                @Override
                public void onClick(DialogInterface dialog, int which)
                {
                    Activity activity = parent.getActivity();
                    if (activity != null)
                    {
                        activity.finish();
                    }
                }
            }).create();
        }
    }
}
