package com.example.shin.myapplication.DatabaseBenchmark;

import android.content.Context;
import android.os.Environment;
import android.util.Log;

import java.io.File;
import java.io.FileOutputStream;
import java.lang.reflect.Method;
import java.util.Collections;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Created by hyesun on 2017-02-10.
 */

public abstract class DataStoreTest
{
    protected Context context;
    protected Map<String, List<Long>> measurements;
    protected final long numberOfObjects;
    protected final long warmupIterations;
    protected final long testIterations;

    protected final String TEST_SIMPLE_QUERY = "SimpleQuery"; // Do a simple query and read 1 field

    public DataStoreTest(Context context, long numberOfObjects, long warmupIterations, long testIterations)
    {
        Log.i("DataStoreBenchmark", this.getClass().getName().toString());
        this.context = context;
        this.measurements = new HashMap<>();
        this.numberOfObjects = numberOfObjects;
        this.warmupIterations = warmupIterations;
        this.testIterations = testIterations;
    }

    protected void setUp()
    {
    }

    protected void tearDown()
    {
        System.gc();
    }

    public void allTests()
    {
        Class clazz = this.getClass();
        Method[] methods = clazz.getMethods();
        for (Method method : methods)
        {
            String name = method.getName();
            if (name.startsWith("test"))
            {
                try
                {
                    Log.i("DataStoreBenchmark", "invoking " + getTag() + ":" + method.getName());
                    method.invoke(this);
                }
                catch (Exception e)
                {
                    throw new RuntimeException(e);
                }
            }
        }
    }


    public abstract void testSimpleQuery();

    protected abstract String getTag();
}