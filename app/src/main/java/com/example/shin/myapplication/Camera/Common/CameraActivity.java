package com.example.shin.myapplication.Camera.Common;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;

import com.example.shin.myapplication.Camera.Camera1.CameraFragment;
import com.example.shin.myapplication.Camera.Camera2.Camera2Fragment;
import com.example.shin.myapplication.MainActivity;
import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.MyApplication;

/**
 * Created by shin on 2017-01-04.
 */

public class CameraActivity extends AppCompatActivity
{
    private final String TAG = "CameraActivity";

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        setContentView(R.layout.activity_camera);
        int version = getIntent().getIntExtra("version", 0);
        Log.i(TAG, "version" + version);

        if (savedInstanceState == null)
        {
            if (version == 1)
            {
                getFragmentManager().beginTransaction().replace(R.id.container, CameraFragment.newInstance()).commit();
            }
            else
            {
                getFragmentManager().beginTransaction().replace(R.id.container, Camera2Fragment.newInstance()).commit();
            }
        }
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        Intent upIntent = new Intent(this, MainActivity.class);
        if (NavUtils.shouldUpRecreateTask(this, upIntent))
        {
            TaskStackBuilder.from(this).addNextIntent(new Intent(this, MainActivity.class)).addNextIntent(upIntent).startActivities();
            finish();
        }
        else
        {
            NavUtils.navigateUpTo(this, upIntent);
        }
        return true;
    }
}
