package com.example.shin.myapplication.Camera.Common;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.media.Image;
import android.media.ThumbnailUtils;
import android.os.Environment;
import android.os.Handler;
import android.os.Message;
import android.view.View;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.CommonValues;
import com.example.shin.myapplication.Utility.ImageName;
import com.example.shin.myapplication.Utility.MyApplication;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.nio.ByteBuffer;

import io.realm.Realm;


/**
 * Created by hyesun on 2017-02-01.
 */

public class ImageSaver implements Runnable, CommonValues
{
    private final String TAG = "ImageSaver";

    //what 0:show 1:hide
    private static final int PROGRESS_SHOW = 0;
    private static final int PROGRESS_HIDE = 1;

    private Context mContext;
    private Activity mActivity;

    private Realm mRealm;

    private File mOriginFile;
    private File mResizeFile;
    private File mThumbFile;

    private Image mOriginImage;
    private byte[] mImageBytes;

    private ProgressBar mProgressBar;

    private int mCameraVersion;
    private String mName;

    // camera1
    public ImageSaver(Context context, byte[] bytes, ProgressBar progressbar, int cameraVersion)
    {
        mContext = context;
        mActivity = (Activity)mContext;

        mRealm = Realm.getDefaultInstance();

        mImageBytes = bytes;

        mCameraVersion = cameraVersion;

        mProgressBar = progressbar;
    }

    // camera2
    public ImageSaver(Context context, Image image, int cameraVersion)
    {
        mContext = context;
        mActivity = (Activity)mContext;

        mOriginImage = image;

        mCameraVersion = cameraVersion;

        mProgressBar = (ProgressBar)mActivity.getWindow().getDecorView().findViewById(R.id.progressbar);
    }

    @Override
    public void run()
    {
        progressHandler.sendEmptyMessage(PROGRESS_SHOW);

        String now = String.valueOf(System.currentTimeMillis());
        now = now.substring(0, now.length() - 2);

        mName = now + ".jpg";

        mOriginFile = new File(FILE_PATH, mName);
        mResizeFile = new File(FILE_PATH, "r_" + mName);
        mThumbFile = new File(FILE_PATH, "t_" + mName);

        File tmpFile = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_DCIM), mName);
        FileOutputStream tmpOutputStream = null;

        for (int i = 0; i < 10000; i++)
        {
            MyApplication.mDBControl.insertImageName(mName);
            insertImageName(mRealm);
        }

        if (mCameraVersion == 2)
        {
            // 이미지 파일 저장
            ByteBuffer buffer = mOriginImage.getPlanes()[0].getBuffer();
            mImageBytes = new byte[buffer.remaining()];
            buffer.get(mImageBytes);
        }

        FileOutputStream originOutput = null;
        FileOutputStream resizeOutput = null;
        FileOutputStream thumbOutput = null;

        // 1920*1080 비트맵 저장
        Bitmap resizeBitmap = BitmapFactory.decodeByteArray(mImageBytes, 0, mImageBytes.length);
        resizeBitmap = ThumbnailUtils.extractThumbnail(resizeBitmap, 1920, 1080);

        // 썸네일 비트맵 저장
        Bitmap thumbBitmap = BitmapFactory.decodeByteArray(mImageBytes, 0, mImageBytes.length);
        thumbBitmap = ThumbnailUtils.extractThumbnail(thumbBitmap, 300, 300);

        try
        {
            originOutput = new FileOutputStream(mOriginFile);
            tmpOutputStream = new FileOutputStream(tmpFile);
            originOutput.write(mImageBytes);
            tmpOutputStream.write(mImageBytes);

            resizeOutput = new FileOutputStream(mResizeFile);
            resizeBitmap.compress(Bitmap.CompressFormat.JPEG, 100, resizeOutput);

            thumbOutput = new FileOutputStream(mThumbFile);
            thumbBitmap.compress(Bitmap.CompressFormat.JPEG, 35, thumbOutput);

            Message msg = Message.obtain();
            msg.what = 1;
            msg.obj = mName;
            progressHandler.sendMessage(msg);

        }
        catch (IOException e)
        {
            e.printStackTrace();
        }
        finally
        {
            if (mCameraVersion == 2)
            {
                mOriginImage.close();
            }

            mOriginFile = null;
            mResizeFile = null;
            mThumbFile = null;

            if (originOutput != null)
            {
                try
                {
                    originOutput.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (resizeOutput != null)
            {
                try
                {
                    resizeOutput.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }
            if (thumbOutput != null)
            {
                try
                {
                    thumbOutput.close();
                }
                catch (IOException e)
                {
                    e.printStackTrace();
                }
            }

            if (mRealm != null)
            {
                mRealm.close();
            }
        }
    }

    private void insertImageName(Realm realm)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Number currentId = realm.where(ImageName.class).max("id");
                int id;
                if (currentId == null)
                {
                    id = 1;
                }
                else
                {
                    id = currentId.intValue() + 1;
                }
                ImageName imageName = realm.createObject(ImageName.class, id);

                imageName.setName(mName);
            }
        });
    }

    Handler progressHandler = new Handler()
    {
        @Override
        public void handleMessage(Message msg)
        {
            switch (msg.what)
            {
                case PROGRESS_SHOW:
                    mProgressBar.bringToFront();
                    mProgressBar.setVisibility(View.VISIBLE);
                    break;
                case PROGRESS_HIDE:
                default:
                    mProgressBar.setVisibility(View.INVISIBLE);
                    Toast.makeText(mContext, "Saved: " + (String)msg.obj, Toast.LENGTH_SHORT).show();
                    break;
            }

            super.handleMessage(msg);
        }
    };
}
