package com.example.shin.myapplication.DatabaseBenchmark;


import android.content.Context;
import android.util.Log;

import com.example.shin.myapplication.Utility.ImageName;
import com.example.shin.myapplication.Utility.MyApplication;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by hyesun on 2017-02-10.
 */

public class TestRealm extends DataStoreTest
{
    private Realm mRealm;

    public TestRealm(Context context, long numberOfObjects, long warmupIterations, long testIterations)
    {
        super(context, numberOfObjects, warmupIterations, testIterations);
    }

    protected void tearDown() {
        super.tearDown();
        mRealm.close();
    }

    protected void setUp()
    {
        super.setUp();
        mRealm = Realm.getDefaultInstance();
    }

    @Override
    public void testSimpleQuery()
    {
        setUp();

        Benchmark benchmark = new Benchmark()
        {
            ArrayList<String> sImageNames = new ArrayList<>();
            @Override
            public void setUp()
            {

            }

            @Override
            public void tearDown()
            {

            }

            @Override
            public void run()
            {
                RealmResults<ImageName> results = mRealm.where(ImageName.class).findAll();
                results = results.sort("name", Sort.DESCENDING);

                for(ImageName result : results)
                {
                    sImageNames.add(result.getName());
                }
            }
        };

        measurements.put(TEST_SIMPLE_QUERY, benchmark.execute(warmupIterations, testIterations));

        Log.i("Realm Time", benchmark.execute(warmupIterations, testIterations) + "");

        tearDown();
    }

    @Override
    protected String getTag() {
        return "realm";
    }
}
