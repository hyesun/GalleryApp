package com.example.shin.myapplication.Gallery.Photo;

import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v4.view.ViewPager;
import android.support.v7.app.AppCompatActivity;

import com.example.shin.myapplication.Gallery.Thumnail.GalleryActivity;
import com.example.shin.myapplication.MainActivity;
import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.DBControl;
import com.example.shin.myapplication.Utility.ImageName;

import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

/**
 * Created by hyesun on 2017-01-17.
 */

public class PhotoActivity extends AppCompatActivity
{
    private ViewPager pager;
    private PhotoAdapter mAdapter;

    private int DBType;

    @Override
    protected void onCreate(Bundle savedInstanceSate)
    {
        super.onCreate(savedInstanceSate);
        //        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_photo);

        Intent intent = getIntent();
        DBType = intent.getIntExtra("DBType", 0);

        pager = (ViewPager)findViewById(R.id.view_pager);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        int position = getIntent().getIntExtra("imagePosition", 0);

        if (DBType == 0 && mAdapter == null)
        {
            mAdapter = new PhotoAdapter(this, getLayoutInflater(), GalleryActivity.rImageNames);
        }
        else if (DBType == 1 && mAdapter == null)
        {
            mAdapter = new PhotoAdapter(this, getLayoutInflater(), GalleryActivity.sImageNames);
        }

        pager.setAdapter(mAdapter);
        pager.setCurrentItem(position);
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (mAdapter != null)
        {
            mAdapter = null;
        }
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        Intent upIntent = new Intent(this, GalleryActivity.class);
        if (NavUtils.shouldUpRecreateTask(this, upIntent))
        {
            TaskStackBuilder.from(this).addNextIntent(new Intent(this, MainActivity.class)).addNextIntent(upIntent).startActivities();
            finish();
        }
        else
        {
            NavUtils.navigateUpTo(this, upIntent);
        }
        return true;
    }
}
