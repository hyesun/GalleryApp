package com.example.shin.myapplication.Gallery.Thumnail;

import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AlertDialog;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.AbsListView;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.GridView;
import android.widget.ProgressBar;
import android.widget.Toast;

import com.example.shin.myapplication.DatabaseBenchmark.TestRealm;
import com.example.shin.myapplication.DatabaseBenchmark.TestSQLite;
import com.example.shin.myapplication.Gallery.Photo.PhotoActivity;
import com.example.shin.myapplication.MainActivity;
import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.ImageName;
import com.example.shin.myapplication.Utility.MyApplication;
import com.example.shin.myapplication.Utility.Util;

import java.io.File;
import java.util.ArrayList;

import io.realm.Realm;
import io.realm.RealmResults;
import io.realm.Sort;

import static com.example.shin.myapplication.Utility.CommonValues.ORIGIN;
import static com.example.shin.myapplication.Utility.CommonValues.RESIZE;
import static com.example.shin.myapplication.Utility.CommonValues.THUMB;

/**
 * Created by shin on 2017-01-03.
 */

public class GalleryActivity extends AppCompatActivity
{
    private String TAG = "Gallery Activity";

    private Context mContext;

    private Realm mRealm;

    private GalleryAdapter mGalleryAdapter;

    private GridView mGridView;
    private Button mAllDeleteButton;
    private ProgressBar mGalleryProgress;

    public static ArrayList<String> sImageNames;
    public static ArrayList<String> rImageNames;
    private int DBType;
    public static int firstPos = 0;
    public static int lastPos = 99999;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        //        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_gallery);

        mContext = this;

        Intent intent = getIntent();
        DBType = intent.getIntExtra("DBType", 0);

        if (DBType == 1)
        {
            //            TestSQLite testSQLite = new TestSQLite(mContext, 50000, 2, 5);
            //            testSQLite.allTests();
            sImageNames = MyApplication.mDBControl.getAllName();
        }
        else
        {
            //            TestRealm testRealm = new TestRealm(mContext, 50000, 2, 5);
            //            testRealm.allTests();
            rImageNames = new ArrayList<>();

            if (mRealm == null || mRealm.isClosed())
            {
                mRealm = Realm.getDefaultInstance();
            }

            getAllNames(mRealm);
        }

        mGridView = (GridView)findViewById(R.id.grid_view);
        mAllDeleteButton = (Button)findViewById(R.id.btn_all_delete);
        mGalleryProgress = (ProgressBar)findViewById(R.id.gallery_progressbar);
    }

    @Override
    protected void onResume()
    {
        super.onResume();

        if (mContext == null)
        {
            mContext = this;
        }

        //        if (DBType == 0)
        //        {
        //            if (mRealm == null || mRealm.isClosed())
        //            {
        //                mRealm = Realm.getDefaultInstance();
        //            }
        //
        //            getAllNames(mRealm);
        //        }

        if (mGalleryAdapter == null)
        {
            mGalleryAdapter = new GalleryAdapter(mContext, DBType);
        }

        mGridView.setAdapter(mGalleryAdapter);

        mGridView.setOnScrollListener(new AbsListView.OnScrollListener()
        {
            @Override
            public void onScrollStateChanged(AbsListView view, int scrollState)
            {

            }

            @Override
            public void onScroll(AbsListView view, int firstVisibleItem, int visibleItemCount, int totalItemCount)
            {
                firstPos = firstVisibleItem;
                lastPos = firstVisibleItem + visibleItemCount;
                //                Log.e(TAG, "first : " + firstVisibleItem + "  final : " + (firstVisibleItem+visibleItemCount));
            }
        });


        mGridView.setOnItemClickListener(new AdapterView.OnItemClickListener()
        {
            @Override
            public void onItemClick(AdapterView<?> parent, View view, int position, long id)
            {
                mGalleryProgress.setVisibility(View.VISIBLE);
                Intent intent = new Intent(getApplicationContext(), PhotoActivity.class);

                intent.putExtra("DBType", DBType);
                intent.putExtra("imagePosition", position);

                startActivity(intent);

            }
        });

        mGalleryProgress.setVisibility(View.INVISIBLE);

        mGridView.setOnItemLongClickListener(new AdapterView.OnItemLongClickListener()
        {
            @Override
            public boolean onItemLongClick(AdapterView<?> parent, View view, final int position, long id)
            {

                Toast.makeText(getApplicationContext(), "Delete : " + mGalleryAdapter.getItem(position), Toast.LENGTH_SHORT).show();
                deleteImage(position);
                mGalleryAdapter.notifyDataSetChanged();

                Realm realm = Realm.getDefaultInstance();
                getAllNames(realm);

                return true;
            }

        });

        mAllDeleteButton.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                AlertDialog.Builder alerDialogBuilder = new AlertDialog.Builder(GalleryActivity.this);
                alerDialogBuilder.setTitle("모두 삭제");
                alerDialogBuilder.setMessage("모두 삭제하시겠습니까?").setPositiveButton("취소", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        dialog.cancel();
                    }
                }).setNegativeButton("삭제", new DialogInterface.OnClickListener()
                {
                    @Override
                    public void onClick(DialogInterface dialog, int which)
                    {
                        if (DBType == 0)
                        {
                            while (rImageNames.size() != 0)
                            {
                                deleteImage(rImageNames.size() - 1);
                            }
                        }
                        else
                        {
                            while (sImageNames.size() != 0)
                            {
                                deleteImage(sImageNames.size() - 1);
                            }
                        }

                        mGalleryAdapter.notifyDataSetChanged();
                    }
                }).show();
            }
        });
    }

    @Override
    protected void onPause()
    {
        super.onPause();

        if (DBType == 0 && mRealm != null)
        {
            mRealm.close();
            mRealm = null;
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();

        if (mContext != null)
        {
            mContext = null;
        }

        if (mGalleryAdapter != null)
        {
            mGalleryAdapter = null;
        }

        sImageNames = null;
        rImageNames = null;
    }

    private void deleteImage(final int position)
    {
        if (DBType == 0)
        {
            deleteImageNames(mRealm, position);

            String types[] = { ORIGIN, RESIZE, THUMB };

            for (int i = 0; i < types.length; i++)
            {
                File tmpFile;
                if (DBType == 0)
                {
                    tmpFile = new File(Util.getImagePath(types[i], rImageNames.get(position)));
                }
                else
                {
                    tmpFile = new File(Util.getImagePath(types[i], sImageNames.get(position)));
                }
                tmpFile.delete();
            }
        }
        else
        {
            MyApplication.mDBControl.deleteImageName(sImageNames.get(position));
        }

        if (DBType == 0)
        {
            Log.i(TAG, "remove");
            rImageNames.remove(position);
        }
        else
        {
            sImageNames.remove(position);
        }

    }

    private void getAllNames(Realm realm)
    {
        RealmResults<ImageName> results = realm.where(ImageName.class).findAll();
        results = results.sort("name", Sort.DESCENDING);

        //        RealmResults<ImageName> results = realm.where(ImageName.class).between("id", 30, 100).findAll();
        //        results = results.sort("name", Sort.DESCENDING);

        for (ImageName result : results)
        {
            rImageNames.add(result.getName());
        }

        //        Log.i(TAG, "image name : " + mImageNames.get(0).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(1).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(2).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(3).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(4).getName());
        //
        //        int size = mImageNames.size();
        //
        //        Log.i(TAG, "image name : " + mImageNames.get(size - 1).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(size - 2).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(size - 3).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(size - 4).getName());
        //        Log.i(TAG, "image name : " + mImageNames.get(size - 5).getName());
    }

    private void deleteImageNames(Realm realm, final int position)
    {
        final RealmResults<ImageName> imageNames = realm.where(ImageName.class).findAll();

        Log.i(TAG, "before : " + imageNames.size());

        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Log.i(TAG, imageNames.get(position).getName());
                imageNames.get(position).deleteFromRealm();
            }
        });
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        Intent upIntent = new Intent(this, MainActivity.class);
        if (NavUtils.shouldUpRecreateTask(this, upIntent))
        {
            TaskStackBuilder.from(this).addNextIntent(new Intent(this, MainActivity.class)).addNextIntent(upIntent).startActivities();
            finish();
        }
        else
        {
            NavUtils.navigateUpTo(this, upIntent);
        }
        return true;
    }

}
