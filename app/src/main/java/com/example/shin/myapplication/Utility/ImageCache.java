package com.example.shin.myapplication.Utility;

import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.util.Log;
import android.util.LruCache;

import java.security.Timestamp;
import java.util.Calendar;

/**
 * Created by hyesun on 2017-01-16.
 */

public class ImageCache implements CommonValues
{
    private static final String TAG = "ImageCache";

    private static LruCache<String, Bitmap> mMemoryCache;
    private static Bitmap bitmap;

    private static void init()
    {
        if (mMemoryCache == null)
        {
            final int maxMemory = (int)(Runtime.getRuntime().maxMemory() / 1024);
            final int cacheSize = maxMemory / 8;

            mMemoryCache = new LruCache<String, Bitmap>(cacheSize)
            {
                @Override
                protected int sizeOf(String key, Bitmap bitmap)
                {
                    return bitmap.getByteCount() / 1024;
                }

            };
        }

    }

    public static Bitmap checkBitmap(final String NAME)
    {
        init();
        if (getBitmapFromMemCache(NAME) == null)
        {
            bitmap = BitmapFactory.decodeFile(Util.getImagePath(THUMB, NAME));

            bitmap.createBitmap(bitmap, 0, 0, 250, 250);

            new Thread(new Runnable()
            {
                @Override
                public void run()
                {
                    addBitmapToMemoryCache(NAME, bitmap);


                    Log.i(TAG, "endTime : " + Calendar.getInstance().getTimeInMillis());
                }
            }).start();

        }
        else
        {
            bitmap = getBitmapFromMemCache(NAME);
        }


        Log.i(TAG, "create count " + mMemoryCache.putCount());

        Log.i(TAG, "startTime : " + Calendar.getInstance().getTimeInMillis());
        return bitmap;
    }

    public static void addBitmapToMemoryCache(String key, Bitmap bitmap)
    {
        init();

        if (getBitmapFromMemCache(key) == null)
        {
            mMemoryCache.put(key, bitmap);
        }

        Log.i(TAG, mMemoryCache.toString());
    }

    public static Bitmap getBitmapFromMemCache(String key)
    {
        init();
        return mMemoryCache.get(key);
    }
}
