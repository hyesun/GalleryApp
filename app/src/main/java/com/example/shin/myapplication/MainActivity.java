package com.example.shin.myapplication;

import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.NavUtils;
import android.support.v4.app.TaskStackBuilder;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.example.shin.myapplication.Camera.Common.CameraActivity;
import com.example.shin.myapplication.Gallery.Thumnail.GalleryActivity;
import com.example.shin.myapplication.SignIn.SignInActivity;

public class MainActivity extends AppCompatActivity implements View.OnClickListener
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
//        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_main);

        Intent intent = getIntent();
        String id = intent.getStringExtra("userId");

//        getSupportActionBar().setTitle(id + "'s Gallery");

        Button btnCamera = (Button)findViewById(R.id.btn_camera);
        btnCamera.setOnClickListener(this);
        Button btnAlbumSqlite = (Button)findViewById(R.id.btn_gallery_sqlite);
        btnAlbumSqlite.setOnClickListener(this);
        Button btnAlbumRealm = (Button)findViewById(R.id.btn_gallery_realm);
        btnAlbumRealm.setOnClickListener(this);
    }

    @Override
    public void onClick(View v)
    {
        Intent intent;
        if (v.getId() == R.id.btn_camera)
        {
            intent = new Intent(this, CameraActivity.class);

            if (Build.VERSION.SDK_INT > Build.VERSION_CODES.M)
            {
                intent.putExtra("version", 2);
            }
            else
            {
                intent.putExtra("version", 1);
            }
            startActivity(intent);
        }
        else if (v.getId() == R.id.btn_gallery_realm)
        {
            intent = new Intent(this, GalleryActivity.class);
            intent.putExtra("DBType", 0);
            startActivity(intent);
        }
        else if (v.getId() == R.id.btn_gallery_sqlite)
        {
            intent = new Intent(this, GalleryActivity.class);
            intent.putExtra("DBType", 1);
            startActivity(intent);
        }
    }

    @Override
    public boolean onSupportNavigateUp()
    {
        Intent upIntent = new Intent(this, GalleryActivity.class);
        if (NavUtils.shouldUpRecreateTask(this, upIntent))
        {
            TaskStackBuilder.from(this).addNextIntent(new Intent(this, SignInActivity.class)).addNextIntent(upIntent).startActivities();
            finish();
        }
        else
        {
            NavUtils.navigateUpTo(this, upIntent);
        }
        return true;
    }

}
