package com.example.shin.myapplication.DatabaseBenchmark;

import android.os.Debug;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by hyesun on 2017-02-10.
 */

public abstract class Benchmark
{

    private List<Long> timings;
    private long timeStart;

    public Benchmark()
    {
        timings = new ArrayList<>();
    }

    /**
     * Setting up the benchmark. This method will only be called once before all the warmup/test iterations.
     */
    public abstract void setUp();

    /**
     * Tear down any structures used by the benchmark. This method will only be called once all warmup/test iterations
     * are completed.
     */
    public abstract void tearDown();

    /**
     * Override this method with your test.
     */
    public abstract void run();

    /**
     * Initialize any data used by a single benchmark run.
     */
    protected void prepareRun()
    {

    }

    /**
     * Cleanup any temporary data created during a single run of the benchmark, e.g inserted objects can be removed.
     */
    protected void cleanupRun()
    {

    }

    public List<Long> execute(long warmupIterations, long numberOfIterations)
    {
        setUp();

        for (int i = 0; i < warmupIterations; i++)
        {
            prepareRun();
            System.gc();
            run();
            cleanupRun();
        }

        for (int i = 0; i < numberOfIterations; i++)
        {
            prepareRun();
            System.gc();
            startTimer();
            run();
            stopTimer();
            cleanupRun();
        }

        tearDown();
        return timings;
    }

    /**
     * Starts the timer
     */
    public void startTimer()
    {
        timeStart = Debug.threadCpuTimeNanos();
    }

    /**
     * Stops the timer
     */
    public void stopTimer()
    {
        long timeStop = Debug.threadCpuTimeNanos();
        long duration = timeStop - timeStart; // may report 0
        timings.add(duration);
    }
}
