package com.example.shin.myapplication.Gallery.Photo;

import android.app.Activity;
import android.content.Context;
import android.graphics.Bitmap;
import android.support.v4.view.PagerAdapter;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.CommonValues;
import com.example.shin.myapplication.Utility.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

/**
 * Created by hyesun on 2017-01-17.
 */

public class PhotoAdapter extends PagerAdapter implements CommonValues
{
    private final String TAG = "Photo Adapter";

    private Context mContext;
    private LayoutInflater inflater;
    private ArrayList<String> mImageNames;

    private int DBType;

    public PhotoAdapter(Context context, LayoutInflater inflater, ArrayList<String> imageNames)
    {
        mContext = context;
        this.inflater = inflater;
        mImageNames = imageNames;
    }

    @Override
    public int getCount()
    {
        return mImageNames.size();
    }

    @Override
    public Object instantiateItem(ViewGroup container, final int position)
    {
        final Activity ACTIVITY = (Activity) mContext;
        View view = null;
        Bitmap bitmap;


        view = inflater.inflate(R.layout.childview_photo, null);

        TextView textView = (TextView)view.findViewById(R.id.image_number);
        textView.setText((position + 1) + " / " + getCount());

        final ImageView imageView = (ImageView)view.findViewById(R.id.image_view);

        ACTIVITY.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                Picasso.with(mContext).load(new File(Util.getImagePath(RESIZE, mImageNames.get(position)))).into(imageView);
            }
        });

        //            bitmap = BitmapFactory.decodeFile(Util.getImagePath(RESIZE, sImagesNames.get(position)));

        //        imageView.setImageBitmap(bitmap);

        container.addView(view);

        return view;
    }

    @Override
    public void destroyItem(ViewGroup container, int position, Object object)
    {
        container.removeView((View)object);
    }

    @Override
    public boolean isViewFromObject(View v, Object object)
    {
        return v == object;
    }
}
