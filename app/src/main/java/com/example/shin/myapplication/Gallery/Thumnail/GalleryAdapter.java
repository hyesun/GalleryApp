package com.example.shin.myapplication.Gallery.Thumnail;

import android.app.Activity;
import android.content.Context;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import android.widget.BaseAdapter;
import android.widget.ImageView;

import com.example.shin.myapplication.Utility.CommonValues;
import com.example.shin.myapplication.Utility.ImageCache;
import com.example.shin.myapplication.Utility.ImageName;
import com.example.shin.myapplication.Utility.MyApplication;
import com.example.shin.myapplication.R;
import com.example.shin.myapplication.Utility.Util;
import com.squareup.picasso.Picasso;

import java.io.File;
import java.util.ArrayList;

import io.realm.Realm;

/**
 * Created by hyesun on 2017-01-04.
 */

public class GalleryAdapter extends BaseAdapter implements CommonValues
{
    private String TAG = "Gallery Adapter";

    private Context mContext;

    private int DBType;

    LayoutInflater inflater;

    public GalleryAdapter(Context context, int type)
    {
        mContext = context;

        DBType = type;
    }


    @Override
    public int getCount()
    {
        if (DBType == 0)
        {
            Log.i(TAG, "size : " + GalleryActivity.rImageNames.size());
            return GalleryActivity.rImageNames.size();
        }
        else
        {
            Log.i(TAG, "size : " + GalleryActivity.sImageNames.size());
            return GalleryActivity.sImageNames.size();
        }
    }

    @Override
    public Object getItem(int position)
    {
        if (DBType == 0)
        {
            return GalleryActivity.rImageNames.get(position);
        }
        else
        {
            return GalleryActivity.sImageNames.get(position);
        }
    }

    @Override
    public long getItemId(int position)
    {
        return position;
    }

    static class ViewHolder
    {
        ImageView imageView;
    }

    ViewHolder holder = null;

    @Override
    public View getView(final int position, View view, ViewGroup viewGroup)
    {
        final Activity ACTIVITY = (Activity)mContext;
        View v = view;

        if (v == null)
        {
            holder = new ViewHolder();
            inflater = (LayoutInflater)mContext.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            v = inflater.inflate(R.layout.gallery_cell, viewGroup, false);
            holder.imageView = (ImageView)v.findViewById(R.id.imageView);

            v.setTag(holder);
        }
        else
        {
            holder = (ViewHolder)v.getTag();
        }

        ACTIVITY.runOnUiThread(new Runnable()
        {
            @Override
            public void run()
            {
                holder.imageView.setPadding(8, 8, 8, 8);
                holder.imageView.setScaleType(ImageView.ScaleType.CENTER_CROP);

                if (DBType == 0)
                {
                    Picasso.with(mContext).load(new File(Util.getImagePath(THUMB, GalleryActivity.rImageNames.get(position)))).into(holder.imageView);
                }
                else
                {
                    Picasso.with(mContext).load(new File(Util.getImagePath(THUMB, GalleryActivity.sImageNames.get(position)))).into(holder.imageView);
                }
            }
        });

        return v;
    }

}
