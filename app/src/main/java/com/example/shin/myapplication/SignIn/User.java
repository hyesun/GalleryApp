package com.example.shin.myapplication.SignIn;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hyesun on 2017-02-07.
 */

public class User extends RealmObject
{
    @PrimaryKey
    private int userNum;
    private String id;
    private String password;

    public int getIndex()
    {
        return userNum;
    }

    public void setIndex(int num)
    {
        this.userNum = num;
    }

    public String getId()
    {
        return id;
    }

    public void setId(String id)
    {
        this.id = id;
    }

    public String getPassword()
    {
        return password;
    }

    public void setPassword(String pwd)
    {
        password = pwd;
    }

}
