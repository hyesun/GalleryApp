package com.example.shin.myapplication.Utility;

import io.realm.RealmObject;
import io.realm.annotations.PrimaryKey;

/**
 * Created by hyesun on 2017-02-06.
 */

public class ImageName extends RealmObject
{
    @PrimaryKey
    private int id;
    private String name;

    public int getId()
    {
        return id;
    }

    public void setId(int id)
    {
        this.id = id;
    }

    public String getName()
    {
        return name;
    }

    public void setName(String name)
    {
        this.name = name;
    }
}
