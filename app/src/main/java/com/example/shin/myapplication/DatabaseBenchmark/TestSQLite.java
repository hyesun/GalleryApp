package com.example.shin.myapplication.DatabaseBenchmark;


import android.content.Context;
import android.util.Log;

import com.example.shin.myapplication.Utility.MyApplication;

import java.util.ArrayList;

/**
 * Created by hyesun on 2017-02-10.
 */

public class TestSQLite extends DataStoreTest
{
    public TestSQLite(Context context, long numberOfObjects, long warmupIterations, long testIterations)
    {
        super(context, numberOfObjects, warmupIterations, testIterations);
    }

    protected void tearDown() {
        super.tearDown();
    }

    protected void setUp()
    {
        super.setUp();
    }

    @Override
    public void testSimpleQuery()
    {
        setUp();

        Benchmark benchmark = new Benchmark()
        {
            ArrayList<String> sImageNames;
            @Override
            public void setUp()
            {

            }

            @Override
            public void tearDown()
            {

            }

            @Override
            public void run()
            {
                sImageNames = MyApplication.mDBControl.getAllName();
            }
        };

        measurements.put(TEST_SIMPLE_QUERY, benchmark.execute(warmupIterations, testIterations));

        Log.i("SQLite Time", benchmark.execute(warmupIterations, testIterations) + "");

        tearDown();
    }

    @Override
    protected String getTag() {
        return "sqlite";
    }
}
