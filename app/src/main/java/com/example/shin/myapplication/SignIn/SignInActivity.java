package com.example.shin.myapplication.SignIn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shin.myapplication.MainActivity;
import com.example.shin.myapplication.R;

import io.realm.Realm;

/**
 * Created by hyesun on 2017-02-07.
 */

public class SignInActivity extends AppCompatActivity implements View.OnClickListener
{
    private final String TAG = "MainActivity";

    private Realm mRealm;

    private EditText editId;
    private EditText editPassword;
    private Button signInButton;
    private Button signUpButton;

    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_sign_in);

        Toolbar toolbar = (Toolbar)findViewById(R.id.toolbar);
        setSupportActionBar(toolbar);


        mRealm = Realm.getDefaultInstance();

        editId = (EditText)findViewById(R.id.edit_id);
        editPassword = (EditText)findViewById(R.id.edit_pwd);
        signInButton = (Button)findViewById(R.id.btn_sign_in);
        signInButton.setOnClickListener(this);
        signUpButton = (Button)findViewById(R.id.btn_sign_up1);
        signUpButton.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        Intent intent;

        if (view.getId() == R.id.btn_sign_in)
        {
            basicCRUD(mRealm);
            if (mUser != null && mUser.getPassword().equals(editPassword.getText().toString()))
            {
                editId.setText("");
                editPassword.setText("");
                intent = new Intent(this, MainActivity.class);
                intent.putExtra("userId", mUser.getId());
                startActivity(intent);
            }
            else
            {
                Toast.makeText(this, "사용자의 정보가 없습니다.", Toast.LENGTH_SHORT).show();
            }

        }
        else if (view.getId() == R.id.btn_sign_up1)
        {
            intent = new Intent(this, SignUpActivity.class);
            startActivity(intent);
        }
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mRealm.close();
    }

    private void basicCRUD(Realm realm)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                mUser = realm.where(User.class).equalTo("id", editId.getText().toString()).findFirst();
            }
        });
    }

}

