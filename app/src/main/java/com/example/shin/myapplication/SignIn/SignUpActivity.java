package com.example.shin.myapplication.SignIn;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.example.shin.myapplication.R;

import io.realm.Realm;

/**
 * Created by hyesun on 2017-02-07.
 */

public class SignUpActivity extends AppCompatActivity implements View.OnClickListener
{
    private Realm mRealm;

    private EditText editId;
    private EditText editPwd;
    private EditText editConfirmPwd;

    private Button btnSignUp;
    private Button btnCancel;

    private User mUser;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);
        setContentView(R.layout.activity_sign_up);

        mRealm = Realm.getDefaultInstance();

        editId = (EditText)findViewById(R.id.edit_id);
        editPwd = (EditText)findViewById(R.id.edit_pwd);
        editConfirmPwd = (EditText)findViewById(R.id.edit_confirm_pwd);

        btnSignUp = (Button)findViewById(R.id.btn_sign_up2);
        btnSignUp.setOnClickListener(this);
        btnCancel = (Button)findViewById(R.id.btn_cancel);
        btnCancel.setOnClickListener(this);
    }

    @Override
    public void onClick(View view)
    {
        Intent intent;
        if (view.getId() == R.id.btn_sign_up2)
        {
            selectUser(mRealm);
            if (mUser != null)
            {
                Toast.makeText(this, "존재하는 ID입니다.", Toast.LENGTH_SHORT).show();
            }
            else if (editPwd.getText().toString().equals(editConfirmPwd.getText().toString()))
            {
                insertUser(mRealm);
                Toast.makeText(this, "회원가입이 완료되었습니다.", Toast.LENGTH_SHORT).show();
            }
        }
        else if (view.getId() == R.id.btn_cancel)
        {
            intent = new Intent(this, SignInActivity.class);
            startActivity(intent);
        }
    }

    private void insertUser(Realm realm)
    {
        realm.executeTransaction(new Realm.Transaction()
        {
            @Override
            public void execute(Realm realm)
            {
                Number currentUserNum = realm.where(User.class).max("userNum");
                int userNum;
                if (currentUserNum == null)
                {
                    userNum = 1;
                }
                else
                {
                    userNum = currentUserNum.intValue() + 1;
                }

                User user = realm.createObject(User.class, userNum);
                user.setId(editId.getText().toString());
                user.setPassword(editPwd.getText().toString());
            }
        });
    }

    private void selectUser(Realm realm)
    {
        mUser = realm.where(User.class).equalTo("id", editId.getText().toString()).findFirst();
    }

    @Override
    protected void onDestroy()
    {
        super.onDestroy();
        mRealm.close();
    }
}

