package com.example.shin.myapplication.Utility;

import android.content.ContentValues;
import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

import java.util.ArrayList;

/**
 * Created by hyesun on 2017-02-07.
 */

public class DBControl extends SQLiteOpenHelper
{
    public String TAG = "DBControl";

    private static final String DB_NAME = "IMAGE.db";
    private static final int DB_VERSION = 3;
    private static final String TABLE_NAME = "NAME_TB";

    public static DBControl instance;

    public DBControl(Context context)
    {
        super(context, DB_NAME, null, DB_VERSION);
    }

    @Override
    public void onUpgrade(SQLiteDatabase db, int oldVersion, int newVersion)
    {

    }

    @Override
    public void onCreate(SQLiteDatabase db)
    {
        String createImagesTable = "CREATE TABLE IF NOT EXISTS '" + TABLE_NAME + "'(id integer primary key autoincrement, name text not null)";
        db.execSQL(createImagesTable);
    }

    public void insertImageName(String name)
    {
        SQLiteDatabase db = getWritableDatabase();

        ContentValues values = new ContentValues();
        values.put("name", name);

        db.insert(TABLE_NAME, null, values);
        db.close();
    }

    public void deleteImageName(String name)
    {
        SQLiteDatabase db = getWritableDatabase();

        String selection = "name LIKE ?";
        String[] selectionArgs = { name };

        db.delete(TABLE_NAME, selection, selectionArgs);
        db.close();
    }

    public ArrayList<String> getAllName()
    {
        SQLiteDatabase db = getReadableDatabase();
        String[] projection = { "id", "name" };
        //        String selection = "id BETWEEN 32 AND 102";
        String sortOrder = "name";

        Cursor cursor = db.query(TABLE_NAME, projection, null, null, null, null, sortOrder);
        //        Cursor cursor = db.query(TABLE_NAME, projection, selection, null, null, null, sortOrder);

        ArrayList<String> names = new ArrayList<String>();
//        ArrayList<Integer> num = new ArrayList<Integer>();

        if (cursor != null)
        {
            int i = 0;
            while (cursor.moveToNext() && i < cursor.getCount())
            {
                names.add(i, cursor.getString(cursor.getColumnIndex("name")));
//                num.add(i++, cursor.getInt(cursor.getColumnIndex("id")));
            }

//            Log.i(TAG, "image name : " + names.get(0));
//            Log.i(TAG, "image name : " + names.get(1));
//            Log.i(TAG, "image name : " + names.get(2));
//            Log.i(TAG, "image name : " + names.get(3));
//            Log.i(TAG, "image name : " + names.get(4));
//
//            int size = names.size();
//            Log.i(TAG, "size = " + size);
//
//            Log.i(TAG, "image name : " + names.get(size - 1));
//            Log.i(TAG, "image name : " + names.get(size - 2));
//            Log.i(TAG, "image name : " + names.get(size - 3));
//            Log.i(TAG, "image name : " + names.get(size - 4));
//            Log.i(TAG, "image name : " + names.get(size - 5));
            db.close();
            return names;
        }
        else
        {
            return null;
        }
    }
}
