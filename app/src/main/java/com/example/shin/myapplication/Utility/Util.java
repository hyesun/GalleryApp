package com.example.shin.myapplication.Utility;

import java.io.File;

/**
 * Created by hyesun on 2017-01-16.
 */

public class Util implements CommonValues
{
    public static String getImagePath(String type, String imageName)
    {
        String path = null;
        if (type.equals(THUMB))
        {
            path = FILE_PATH + File.separator + "t_" + imageName;
        }
        else if (type.equals(ORIGIN))
        {
            path = FILE_PATH + File.separator + imageName;
        }
        else if (type.equals(RESIZE))
        {
            path = FILE_PATH + File.separator + "r_" + imageName;
        }
        return path;
    }
}
